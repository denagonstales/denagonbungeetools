package me.denagon.bungee.tools;

import me.denagon.bungee.tools.commands.*;
import me.denagon.bungee.tools.listeners.*;
import me.denagon.bungee.tools.player.AcPlayerManager;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Main extends Plugin
{
    private static final String VERSION = "2.0-BETA";
    private static Main instance;
    private AcPlayerManager playerManager;
    private Configuration config;
    private Configuration lang;
    private String btLayout;
    private boolean bungeePerms;
    private boolean keepLog;
    private Path logPath;

    public static Main getInstance()
    {
        return instance;
    }

    public static String getVersion()
    {
        return VERSION;
    }

    @Override
    public void onLoad()
    {
        setupConfig();
    }

    @Override
    public void onEnable()
    {
    	
        System.out.println(">>>>>>>> [INFO] ----------------- DenagonBungeeTools v1.0 Alpha -----------------");
        System.out.println(">>>>>>>> [INFO] [DBgTools]  DenagonBungeeTools by Fabrimat has been enabled!");
        System.out.println(">>>>>>>> [INFO] [DBgTools]  Thank you for using this Plugin!");
        System.out.println(">>>>>>>> [INFO] ------------------- By Fabrimat & Cekkin23 ----------------------");
    	
        instance = this;
        playerManager = new AcPlayerManager();

        getProxy().getPluginManager().registerCommand(this, new Ac());
        getProxy().getPluginManager().registerCommand(this, new BtReload());
        getProxy().getPluginManager().registerCommand(this, new PingAll());
        getProxy().getPluginManager().registerCommand(this, new CommandSpy());
        getProxy().getPluginManager().registerListener(this, new PlayerListener());
        getProxy().getPluginManager().registerListener(this, new ForgeListener());
        getProxy().getPluginManager().registerListener(this, new CommandListener());
    }

    private void setupConfig()
    {
        Path dirPath = getDataFolder().toPath();
        Path configPath = Paths.get(dirPath.toAbsolutePath().toString(), "config.yml");
        Path langPath = Paths.get(dirPath.toAbsolutePath().toString(), "lang.yml");

        if (!Files.exists(dirPath))
        {
            try
            {
                Files.createDirectory(dirPath);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }

        if (!Files.exists(configPath))
        {
            try
            {
                Files.copy(getResourceAsStream("config.yml"), configPath);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }

        if (!Files.exists(langPath))
        {
            try
            {
                Files.copy(getResourceAsStream("lang.yml"), langPath);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }

        try
        {
            config = YamlConfiguration.getProvider(YamlConfiguration.class).load(configPath.toFile());
            lang = YamlConfiguration.getProvider(YamlConfiguration.class).load(langPath.toFile());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        // Load configuration settings
        btLayout = getConfig().getString("bt-layout");
        keepLog = getConfig().getBoolean("keep-log", false);

        if (keepLog)
        {
            logPath = Paths.get(dirPath.toAbsolutePath().toString(), "bungeetools.log");

            if (!Files.exists(logPath))
            {
                try
                {
                    Files.createFile(logPath);
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }
    }

    public Configuration getConfig()
    {
        return config;
    }

    public Configuration getLang()
    {
        return lang;
    }

    public String getBtLayout()
    {
        return ChatColor.translateAlternateColorCodes('&', btLayout);
    }

    public AcPlayerManager getPlayerManager()
    {
        return playerManager;
    }

    public boolean isBungeePerms()
    {
        return bungeePerms;
    }

    public boolean keepLog()
    {
        return keepLog;
    }

    public void appendToLog(String line)
    {
        try
        {
            BufferedWriter out = Files.newBufferedWriter(logPath, Charset.defaultCharset(),
                                                         StandardOpenOption.APPEND);
            out.append(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())).append(": ").append(line);
            out.newLine();
            out.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void reload()
    {
        try
        {
            config = YamlConfiguration.getProvider(YamlConfiguration.class).load(Paths.get(
                    getDataFolder().getAbsolutePath(), "config.yml").toFile());
            lang = YamlConfiguration.getProvider(YamlConfiguration.class)
                                    .load(Paths.get(getDataFolder().getAbsolutePath(), "lang.yml").toFile());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        // Load configuration settings
        btLayout = getConfig().getString("bt-layout");
        keepLog = getConfig().getBoolean("keep-log", false);

        if (keepLog)
        {
            logPath = Paths.get(getDataFolder().toPath().toAbsolutePath().toString(), "bungeetools.log");

            if (!Files.exists(logPath))
            {
                try
                {
                    Files.createFile(logPath);
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }
    }
}
