package me.denagon.bungee.tools.commands;

import me.denagon.bungee.tools.Main;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.plugin.Command;


public class BtReload extends Command
{
    public BtReload()
    {
        super("btreload");
    }

    @Override
    public void execute(CommandSender sender, String[] args)
    {
        if (sender.hasPermission("bt.reload") || sender.hasPermission("bt.*"))
        {
            Main.getInstance().reload();

            sender.sendMessage(new ComponentBuilder(ChatColor.translateAlternateColorCodes('&',
            		Main.getInstance()
                                                                                                          .getLang()
                                                                                                          .getString(
                                                                                                                  "bt-reload")))
                                       .create());
        }
        else
        {
            sender.sendMessage(new ComponentBuilder(ChatColor.translateAlternateColorCodes('&',
            		Main.getInstance()
                                                                                                          .getLang()
                                                                                                          .getString(
                                                                                                                  "no-permission")))
                                       .create());
        }
    }
}
