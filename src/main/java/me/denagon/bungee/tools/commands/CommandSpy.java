package me.denagon.bungee.tools.commands;

import java.util.ArrayList;
import java.util.List;

import me.denagon.bungee.tools.Main;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class CommandSpy extends Command{
    public CommandSpy()
    {
        super("commandspy", "commandspy.use");
    }

	@SuppressWarnings("unchecked")
	@Override
	public void execute(CommandSender sender, String[] args) {
		int nArgs = args.length;
		if (nArgs == 0){
			sender.sendMessage(new ComponentBuilder(ChatColor.translateAlternateColorCodes('&',
            		Main.getInstance().getLang().getString("invalid-args"))).create());
		} else if (nArgs == 1) {
			if (args[0] == "toggle"){
				 List<String> users = new ArrayList(Main.getInstance().getConfig().getList("commandspy-list"));
				 if (users.contains(((ProxiedPlayer) sender).getUniqueId())){
					 users.remove(((ProxiedPlayer) sender).getUniqueId().toString());
					 Main.getInstance().getConfig().set("commandspy-enabled", users);
				 } else {
					 users.add(((ProxiedPlayer) sender).getUniqueId().toString());
					 Main.getInstance().getConfig().set("commandspy-enabled", users);
				 }
			} else if (args[0] == "check"){
				List<String> users = new ArrayList(Main.getInstance().getConfig().getList("commandspy-list"));
				if (users.contains(((ProxiedPlayer) sender).getUniqueId())){
					sender.sendMessage(new ComponentBuilder(ChatColor.translateAlternateColorCodes('&',
		            		Main.getInstance().getLang().getString("cmdspy-enabled"))).create());
				} else {
					sender.sendMessage(new ComponentBuilder(ChatColor.translateAlternateColorCodes('&',
		            		Main.getInstance().getLang().getString("cmdspy-disabled"))).create());
				}
			} else {
				sender.sendMessage(new ComponentBuilder(ChatColor.translateAlternateColorCodes('&',
	            		Main.getInstance().getLang().getString("invalid-args"))).create());
			}
		} else {
			sender.sendMessage(new ComponentBuilder(ChatColor.translateAlternateColorCodes('&',
            		Main.getInstance().getLang().getString("invalid-args"))).create());
		}
		
	}
}
