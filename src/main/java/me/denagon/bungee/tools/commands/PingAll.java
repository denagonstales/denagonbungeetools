package me.denagon.bungee.tools.commands;

import me.denagon.bungee.tools.Main;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;



public class PingAll extends Command
{
    public PingAll()
    {
        super("pingall");
    }

    @Override
    public void execute(CommandSender sender, String[] args)
    {
        if (sender.hasPermission("at.pingall") || sender.hasPermission("at.*"))
        {
        	for (ProxiedPlayer player : Main.getInstance().getProxy().getPlayers())
            {
                int playerPing = player.getPing();
                String playerDisplayName = player.getDisplayName();
                TextComponent message = new TextComponent(playerDisplayName + " : " + playerPing + " ms");
                sender.sendMessage(message);
            }
        }
        else
        {
            sender.sendMessage(new ComponentBuilder(ChatColor.translateAlternateColorCodes('&',
            		Main.getInstance().getLang().getString("no-permission"))).create());
        }
    }
}
