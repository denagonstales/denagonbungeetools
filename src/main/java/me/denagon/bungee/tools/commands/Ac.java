package me.denagon.bungee.tools.commands;

import me.denagon.bungee.tools.Main;
import me.denagon.bungee.tools.player.AcPlayer;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.chat.TextComponent;

import java.util.regex.Matcher;

public class Ac extends Command
{
    public Ac()
    {
        super("ac");
    }

	@Override
    public void execute(CommandSender sender, String[] args)
    {
        if (sender.hasPermission("bt.ac.use") || sender.hasPermission("bt.*"))
        {
        	if (args.length != 0)
        	{
	            StringBuilder msgBuilder = new StringBuilder();
	            for (String arg : args)
	            {
	                msgBuilder.append(arg).append(" ");
	            }
	
	            String msg = msgBuilder.toString();
	
	            if (sender.hasPermission("bt.ac.format") || sender.hasPermission("bt.*"))
	            {
	                msg = ChatColor.translateAlternateColorCodes('&', msg);
	            }
	
	            String target = sender instanceof ProxiedPlayer ? sender.getName() : "CONSOLE";
	            String server = sender instanceof ProxiedPlayer ? ((ProxiedPlayer) sender).getServer().getInfo().getName() : "N/A";
	
	            String acMessage = Main.getInstance().getBtLayout().replaceAll("%message%", Matcher.quoteReplacement(msg));
	
	            if (Main.getInstance().isBungeePerms())
	            {
	                if (sender instanceof ProxiedPlayer)
	                {
	                    acMessage = acMessage.replaceAll("%player%",Matcher.quoteReplacement(target));
	                }
	                else
	                {
	                    acMessage = acMessage.replaceAll("%player%", "CONSOLE");
	                }
	            }
	            
	            TextComponent message = new TextComponent(acMessage);
	            
	            message.setHoverEvent( new HoverEvent( HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Server: " + Matcher.quoteReplacement(server)).color( ChatColor.RED ).create() ) );
	            
	            for (ProxiedPlayer player : Main.getInstance().getProxy().getPlayers())
	            {
	                if (player.hasPermission("bt.ac.use") || player.hasPermission("bt.*"))
	                {	
	                    player.sendMessage(message);
	                }
	            }
	
	            if (Main.getInstance().keepLog())
	            {
	            	Main.getInstance().appendToLog(ChatColor.stripColor(acMessage));
	            }
	
	            return;
        	} else {
        		if (!(sender instanceof ProxiedPlayer))
                {
                    sender.sendMessage(new ComponentBuilder("This command is player only.").create());
                    return;
                }

                ProxiedPlayer pp = (ProxiedPlayer) sender;
                AcPlayer player = Main.getInstance().getPlayerManager().getPlayer(pp.getUniqueId());

                if (pp.hasPermission("bt.ac.use") || pp.hasPermission("bt.*"))
                {
                    if (player.isAcToggled())
                    {
                        pp.sendMessage(new ComponentBuilder(ChatColor.translateAlternateColorCodes('&',
                                                                                                   Main.getInstance()
                                                                                                                  .getLang()
                                                                                                                  .getString(
                                                                                                                          "ac-toggle-disable")))
                                               .create());
                    }
                    else
                    {
                        pp.sendMessage(new ComponentBuilder(ChatColor.translateAlternateColorCodes('&',
                        		Main.getInstance()
                                                                                                                  .getLang()
                                                                                                                  .getString(
                                                                                                                          "ac-toggle-enable")))
                                               .create());
                    }

                    player.setAcToggled(!player.isAcToggled());
                }
                else
                {
                    sender.sendMessage(new ComponentBuilder(ChatColor.translateAlternateColorCodes('&',
                    		Main.getInstance()
                                                                                                                  .getLang()
                                                                                                                  .getString(
                                                                                                                          "no-permission")))
                                               .create());
                }
        	}
        }

        sender.sendMessage(new ComponentBuilder(ChatColor.translateAlternateColorCodes('&',
                                                                                       Main.getInstance()
                                                                                                      .getLang()
                                                                                                      .getString(
                                                                                                              "no-permission"))).create());
    }
}

