package me.denagon.bungee.tools.player;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class AcPlayerManager
{
    private Set<AcPlayer> players;

    public AcPlayerManager()
    {
        players = new HashSet<AcPlayer>();
    }

    public AcPlayer getPlayer(UUID uuid)
    {
        for (AcPlayer player : players)
        {
            if (player.getUniqueId().equals(uuid))
            {
                return player;
            }
        }

        return null;
    }

    public void addPlayer(UUID uuid)
    {
        players.add(new AcPlayer(uuid));
    }

    public void removePlayer(UUID uuid)
    {
        players.remove(getPlayer(uuid));
    }

    public Set<AcPlayer> getPlayers()
    {
        return players;
    }
}
