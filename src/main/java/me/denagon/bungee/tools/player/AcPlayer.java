package me.denagon.bungee.tools.player;

import java.util.UUID;

public class AcPlayer
{
    private UUID uniqueId;
    private boolean acToggled;

    public AcPlayer(UUID uniqueId)
    {
        this.uniqueId = uniqueId;
        this.acToggled = false;
    }

    public UUID getUniqueId()
    {
        return uniqueId;
    }

    public boolean isAcToggled()
    {
        return acToggled;
    }

    public void setAcToggled(boolean acToggled)
    {
        this.acToggled = acToggled;
    }
}

