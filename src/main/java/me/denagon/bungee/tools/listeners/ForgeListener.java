package me.denagon.bungee.tools.listeners;

import me.denagon.bungee.tools.Main;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class ForgeListener implements Listener{
	@EventHandler
    public void onPlayerConnect(PostLoginEvent e)
    {
		if (!Main.getInstance().getConfig().getBoolean("forge-can-join"))
			if (e.getPlayer().isForgeUser()){
				e.getPlayer().disconnect(new ComponentBuilder(ChatColor.translateAlternateColorCodes('&',
            		Main.getInstance().getLang().getString("disconnect-no-forge"))).create());
			}
    }
}
