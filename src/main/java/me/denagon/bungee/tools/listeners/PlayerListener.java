package me.denagon.bungee.tools.listeners;

import me.denagon.bungee.tools.Main;
import me.denagon.bungee.tools.player.AcPlayer;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.event.ServerKickEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class PlayerListener implements Listener
{
    @EventHandler
    public void onPlayerConnect(PostLoginEvent e)
    {
        Main.getInstance().getPlayerManager().addPlayer(e.getPlayer().getUniqueId());
    }

    @EventHandler
    public void onPlayerDisconnect(PlayerDisconnectEvent e)
    {
    	Main.getInstance().getPlayerManager().removePlayer(e.getPlayer().getUniqueId());
    }

    @EventHandler
    public void onPlayerChat(ChatEvent e)
    {
        if (e.isCommand())
        {
            return;
        }

        ProxiedPlayer pp = (ProxiedPlayer) e.getSender();

        if (pp.hasPermission("at.toggle") || pp.hasPermission("at.*"))
        {
            AcPlayer player = Main.getInstance().getPlayerManager().getPlayer(pp.getUniqueId());

            if (player.isAcToggled())
            {
                e.setCancelled(true);

                Main.getInstance().getProxy().getPluginManager().dispatchCommand(pp, "at " + e.getMessage());
                return;
            }
        }
    }
    @SuppressWarnings("deprecation")
	@EventHandler
    public void onPlayerKick(ServerKickEvent e) {
        if (e.getPlayer().getServer().getInfo().getName() != Main.getInstance().getConfig().getString("default-server")) {
            if (!e.getKickReason().startsWith("\u00a70")) {
                e.setCancelled(true);
                String message = String.valueOf(Main.getInstance().getConfig().getString("kick-prefix")) + " " + (Object)ChatColor.RESET + e.getKickReason();
                message = ChatColor.translateAlternateColorCodes('&', message);
                TextComponent msg = new TextComponent(message);
                e.getPlayer().sendMessage(msg);
            }
        } else {
            e.setKickReason((Object)ChatColor.RED + e.getKickReason());
        }
    }
}

