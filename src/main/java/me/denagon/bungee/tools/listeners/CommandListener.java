package me.denagon.bungee.tools.listeners;

import me.denagon.bungee.tools.Main;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;

public class CommandListener implements Listener{
	@SuppressWarnings("deprecation")
	public void onPlayerChat(ChatEvent e)
    {
        if (e.isCommand())
        {
        	for (ProxiedPlayer player : Main.getInstance().getProxy().getPlayers())
            {
                if (player.hasPermission("bt.cmdspy.see") || player.hasPermission("bt.*"))
                {	
                    player.sendMessage(e.getMessage());
                }
            }
        }
    }
}
